<?php
namespace libClass;

class LibClass
{
    const PRINTFOR3 = 'Linio';
    const PRINTFOR5 = 'IT';
    const PRINTFORBOTH = 'Linianos';

    /**
     * print Linio if the number is divisible by 3
     */
    protected static function printFor3(): string
    {
        return self::PRINTFOR3;
    }

    /**
     * Prints IT if the number is divisible by 5
     */
    protected static function printFor5(): string
    {
        return self::PRINTFOR5;
    }

   /**
    * Prints Linianos if the number is divisble by both 3 and 5
    */
    protected static function printForBoth(): string
    {
        return self::PRINTFORBOTH;
    }

    /**
     * Method to check are the number is divisible by  3 or 5 or both
     *
     * @param integer $number
     */
    public static function checkmode(int $number): string
    {
        switch ($number) {
            case ($number % 3 == 0  && $number % 5 == 0) :
                return self::printForBoth();
                break;
            case ($number % 3 == 0)  :
                return self::printFor3();
                break;
            case ($number % 5 == 0) :                
                return self::printFor5();
                break;
            default :
                return $number;
                break;
        }
    } // End checkmode
} // End Class
