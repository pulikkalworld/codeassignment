<?php

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Error\Error;

require_once dirname(__FILE__) . "/../../src/PrintNumbers.php";

class PrintNumbersTest extends TestCase {
    
    protected $print;
    
    /**
     * test for startPrinting method
     * @covers PrintNumbers::startPrinting
     */    
    public function testStartPrinting()
    {
        $startPoint = 1;
        $endpoint = 100;

        $this->print = new PrintNumbers($startPoint, $endpoint);
        $expectedReturnValue = $this->print->startPrinting();
        $this->assertNull($expectedReturnValue);
    }
}