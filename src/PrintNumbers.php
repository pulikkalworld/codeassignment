<?php
declare(strict_types=1);

use libClass as lib;

require_once dirname(__FILE__) . '/libClass.php';

/**
 * A Class to print the numbers from 1 to 100
 */
class PrintNumbers
{
    protected $startPoint;
    protected $endPoint;
	
	/**
	 * Constuctor function with two paramter for starting and ending number to print
	 *
	 * @param integer $startPoint
	 * @param integer $endPoint
	 */
	public function __construct(int $startPoint, int $endPoint)
	{
		
        if(! is_int($startPoint) || ! is_int($endPoint)){
			throw new \InvalidArgumentException('Expected the integer ');
		}
        $this->startPoint = $startPoint;
        $this->endPoint = $endPoint;
	}
	
	/**
	 * A method which will print the numbers
	 *
	 * @return void
	 */
	public function startPrinting(): void
	{			
		while($this->startPoint <= $this->endPoint)
		{
			echo lib\LibClass::checkmode($this->startPoint). "</br>";	
			$this->startPoint++;		
		}
	}//End function StartPrinting	
}//End Class

